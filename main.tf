provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12"

  backend "s3" {
    bucket     = "gadevs-tfstate"
    key        = "test/terraform-asg.tfstate"
    region     = "ap-southeast-2"
    lock_table = "terraform-lock"
  }
}

module "vpc" {
  source         = "./vpc"
  stack_name     = var.stack_name
  environment    = var.environment
  owner          = var.owner
  key_name       = var.key_name
  nat_gw_count   = "1"
  enable_jumpbox = 1
}

module "asg" {
  source                = "./asg"
  stack_name            = var.stack_name
  environment           = var.environment
  owner                 = var.owner
  key_name              = var.key_name
  public_subnet_ids     = module.vpc.public_subnet_ids[0]
  private_subnet_ids    = module.vpc.private_subnet_ids[0]
  http_inbound_sg_id    = module.vpc.elb_http_inbound_sg_id
  https_inbound_sg_id   = module.vpc.elb_https_inbound_sg_id
  app_ssh_inbound_sg_id = module.vpc.app_ssh_inbound_sg_id
  outbound_sg_id        = module.vpc.elb_outbound_sg_id

  listeners = {
    "instance_port"     = "80"
    "instance_protocol" = "HTTP"
    "lb_port"           = "80"
    "lb_protocol"       = "HTTP"
  }
}

