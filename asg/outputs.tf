output "asg_id" {
  value = aws_autoscaling_group.asg.id
}

output "elb_name" {
  value = aws_elb.elb_two.name
}

output "elb_dns_name" {
  # Return the dns_name of whichever elb was created
  value = aws_elb.elb_two.dns_name
}

output "elb_dns_hosted_zone" {
  # Return the zone_id of whichever elb was created
  value = aws_elb.elb_two.zone_id
}

output "webapp_lc_id" {
  value = aws_launch_configuration.lc.id
}

output "webapp_lc_name" {
  value = aws_launch_configuration.lc.name
}

